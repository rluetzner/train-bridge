package errors

import (
	"errors"
	"fmt"
)

type TbError struct {
	message      string
	innerError   error
	functionName string
}

func New(message, functionName string) *TbError {
	return &TbError{
		message:      message,
		functionName: functionName,
	}
}

func Wrap(message, functionName string, err error) *TbError {
	return &TbError{
		message:      message,
		functionName: functionName,
		innerError:   err,
	}
}

func (e *TbError) Error() string {
	var msg string
	if e.innerError != nil {
		msg = fmt.Sprintf("%s: %s", e.message, e.innerError.Error())
	} else {
		msg = e.message
	}
	return msg
}

func (e *TbError) GetStackTrace() string {
	var stackTrace string
	var innerErr *TbError
	if errors.As(e.innerError, &innerErr) {
		stackTrace = fmt.Sprintf("%s/%s", e.functionName, innerErr.GetStackTrace())
	} else {
		stackTrace = e.functionName
	}
	return stackTrace
}

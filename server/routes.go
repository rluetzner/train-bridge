package server

import "net/http"

func (s *Server) Routes() {
	fs := http.FileServer(http.Dir(s.StaticFilesDir))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.HandleFunc("/", s.logRequest(s.handleErrors(s.serveTemplate)))
}

package server

import (
	"fmt"
	"html/template"
	"net/http"
	"path"
	"sort"
	"time"

	"codeberg.org/rluetzner/train-bridge/api"
	"codeberg.org/rluetzner/train-bridge/errors"
	"codeberg.org/rluetzner/train-bridge/logger"
	"codeberg.org/rluetzner/train-bridge/models"
)

type Server struct {
	DbApiClient    api.DbApiClient
	Settings       *models.Settings
	TemplateFile   string
	StaticFilesDir string
}

func (s *Server) Start() {
	const functionName = "Start"
	port := ":8080"
	logger.Info(fmt.Sprintf("Start listening on %s...", port))

	if err := http.ListenAndServe(port, nil); err != nil {
		logger.Fatal(errors.Wrap("server crashed", functionName, err))
	}
}

func (s *Server) serveTemplate(w http.ResponseWriter, r *http.Request) error {
	const functionName = "ServeTemplate"

	trains, err := s.getUpcomingTrains()
	if err != nil {
		return errors.Wrap("unable to get upcoming trains", functionName, err)
	}

	data := &models.TemplateData{
		TrainStation: s.Settings.TrainStation(),
		NextStation:  s.Settings.NextStation(),
		Trains:       trains,
	}

	name := path.Base(s.TemplateFile)
	tmpl := template.Must(template.New(name).ParseFiles(s.TemplateFile))
	if err := tmpl.Execute(w, data); err != nil {
		return errors.Wrap("error while executing template", functionName, err)
	}

	return nil
}

func (s *Server) handleErrors(f func(http.ResponseWriter, *http.Request) error) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		err := f(w, r)
		if err != nil {
			logger.Error(err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
	}
}

func (s *Server) logRequest(f func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		defer func() {
			duration := time.Since(start)
			logger.Info(fmt.Sprintf("request with URI '%s' took %s", r.RequestURI, duration))
		}()
		f(w, r)
	}
}

func (s *Server) getUpcomingTrains() ([]*models.Train, error) {
	const functionName = "getUpcomingTrains"
	now := time.Now()
	date := now.Format(models.DateFormat)
	hour := fmt.Sprintf("%02d", now.Hour())

	// In an hour might be tomorrow.
	inAnHour := now.Add(1 * time.Hour)
	nextDate := inAnHour.Format(models.DateFormat)
	nextHour := fmt.Sprintf("%02d", inAnHour.Hour())

	// Get trains arriving and departing in the current and next hour.
	trains, err := s.DbApiClient.GetTrains(date, hour, s.Settings.EvaNr(), s.Settings.NextStation())
	if err != nil {
		return nil, errors.Wrap("unable to get trains", functionName, err)
	}

	trainsForNextHour, err := s.DbApiClient.GetTrains(nextDate, nextHour, s.Settings.EvaNr(), s.Settings.NextStation())
	if err != nil {
		return nil, errors.Wrap("unable to get second set of trains", functionName, err)
	}

	trains = append(trains, trainsForNextHour...)

	// The API seems to return some redundant data.
	// This might be caused by the two separate requests above.
	trains = unique(trains)
	trains = filterPastTrains(trains)

	// We want the trains sorted by time.
	sort.Slice(trains, func(i, j int) bool {
		return trains[i].Time < trains[j].Time
	})
	return trains, nil
}

func unique(trains []*models.Train) []*models.Train {
	keys := make(map[string]bool)
	uniqueTrains := make([]*models.Train, 0)
	for _, t := range trains {
		if _, added := keys[t.Time]; !added {
			keys[t.Time] = true
			uniqueTrains = append(uniqueTrains, t)
		}
	}
	return uniqueTrains
}

func filterPastTrains(trains []*models.Train) []*models.Train {
	twoMinutesAgo := time.Now().Add(-2 * time.Minute).Format(api.SaneDateFormat)
	upcomingTrains := make([]*models.Train, 0)
	for _, t := range trains {
		if t.Time >= twoMinutesAgo {
			upcomingTrains = append(upcomingTrains, t)
		}
	}

	return upcomingTrains
}

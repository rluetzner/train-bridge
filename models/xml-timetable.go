package models

import (
	"encoding/xml"
)

type Connection struct {
	Text            string `xml:",chardata"`
	PlannedTime     string `xml:"pt,attr"`
	PlannedPlatform string `xml:"pp,attr"`
	L               string `xml:"l,attr"`
	PlannedPath     string `xml:"ppth,attr"`
	Wings           string `xml:"wings,attr"`
}

// Generated with https://www.onlinetool.io/xmltogo/

type Timetable struct {
	XMLName xml.Name `xml:"timetable"`
	Text    string   `xml:",chardata"`
	Station string   `xml:"station,attr"`
	Stops   []struct {
		Text      string `xml:",chardata"`
		ID        string `xml:"id,attr"`
		TripLabel struct {
			Text string `xml:",chardata"`
			F    string `xml:"f,attr"`
			T    string `xml:"t,attr"`
			O    string `xml:"o,attr"`
			C    string `xml:"c,attr"`
			N    string `xml:"n,attr"`
		} `xml:"tl"`
		Arrival struct {
			Connection
			Pde string `xml:"pde,attr"`
		} `xml:"ar"`
		Departure struct {
			Connection
		} `xml:"dp"`
	} `xml:"s"`
}

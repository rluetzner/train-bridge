package models

type Settings struct {
	evaNr        string
	trainStation string
	nextStation  string
}

func NewSettings(evaNr, trainStation, nextStation string) *Settings {
	return &Settings{
		evaNr:        evaNr,
		trainStation: trainStation,
		nextStation:  nextStation,
	}
}

func (s *Settings) EvaNr() string {
	return s.evaNr
}

func (s *Settings) TrainStation() string {
	return s.trainStation
}

func (s *Settings) NextStation() string {
	return s.nextStation
}

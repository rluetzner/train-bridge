package models

import (
	"strings"

	"github.com/caarlos0/env"
	log "github.com/sirupsen/logrus"
)

type ClientCredentials struct {
	ClientId     string `env:"TB_CLIENT_ID,required"`
	ClientSecret string `env:"TB_CLIENT_SECRET,required"`
}

type EnvVars struct {
	ClientId     string `env:"TB_CLIENT_ID,required"`
	ClientSecret string `env:"TB_CLIENT_SECRET,required"`
	EvaNr        string `env:"TB_EVA_NR,required"`
	NextStation  string `env:"TB_NEXT_STATION,required"`
}

func (e *EnvVars) Credentials() *ClientCredentials {
	return &ClientCredentials{
		ClientId:     e.ClientId,
		ClientSecret: e.ClientSecret,
	}
}

func GetEnvVars() (*EnvVars, error) {
	var cfg EnvVars
	err := env.Parse(&cfg)
	return &cfg, err
}

type LogLevel struct {
	LogLevel string `env:"TB_LOG_LEVEL"`
}

// GetLogLevel checks if the env variable is set and parses
// the log level from it.
// It returns the parsed log level and a boolean indicating
// that it was successfully parsed. 'false' means that the
// default log level was returned.
func GetLogLevel() (log.Level, bool) {
	var l LogLevel
	err := env.Parse(&l)
	if err != nil {
		return log.InfoLevel, false
	}

	if l.LogLevel == "" {
		return log.InfoLevel, false
	}
	switch strings.ToLower(l.LogLevel) {
	case "trace":
		return log.TraceLevel, true
	case "debug":
		return log.DebugLevel, true
	case "info":
		return log.InfoLevel, true
	case "warn":
		return log.WarnLevel, true
	case "error":
		return log.ErrorLevel, true
	case "fatal":
		return log.FatalLevel, true
	case "panic":
		return log.PanicLevel, true
	default:
		log.Warnf("Log level could not be parsed from '%s'. Setting default.", l.LogLevel)
		return log.InfoLevel, false
	}
}

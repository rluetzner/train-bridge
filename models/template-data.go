package models

type TemplateData struct {
	TrainStation string
	NextStation  string
	Trains       []*Train
}

type Train struct {
	Time      string
	Path      string
	IsArrival bool
}

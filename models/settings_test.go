package models

import "testing"

func TestProperties(t *testing.T) {
	eva := "eva-id"
	station := "station"
	nextStation := "next-station"
	sett := NewSettings(eva, station, nextStation)
	if sett.EvaNr() != eva {
		t.Errorf("Expected eva %s, but got %s", eva, sett.EvaNr())
	}
	if sett.TrainStation() != station {
		t.Errorf("Expected train station %s, but got %s", station, sett.TrainStation())
	}
	if sett.NextStation() != nextStation {
		t.Errorf("Expected next station %s, but got %s", nextStation, sett.NextStation())
	}
}

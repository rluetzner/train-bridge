package models

import "encoding/xml"

// Generated with https://www.onlinetool.io/xmltogo/

type Stations struct {
	XMLName xml.Name `xml:"stations"`
	Text    string   `xml:",chardata"`
	Station []struct {
		Text       string `xml:",chardata"`
		Name       string `xml:"name,attr"`
		Eva        string `xml:"eva,attr"`
		Ds100      string `xml:"ds100,attr"`
		Db         string `xml:"db,attr"`
		Creationts string `xml:"creationts,attr"`
		Meta       string `xml:"meta,attr"`
	} `xml:"station"`
}

type Station struct {
	Name  string
	EvaNr string
}

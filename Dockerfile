FROM golang:latest AS builder

COPY go.mod go.sum /app/
WORKDIR /app
RUN go mod download
COPY . /app/
RUN CGO_ENABLED=0 go build -o train-bridge

FROM scratch

COPY --from=builder /app/ /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

EXPOSE 8080/tcp
ENTRYPOINT [ "/train-bridge" ]

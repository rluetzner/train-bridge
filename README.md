# Train-Bridge

<img src="static/train.png" width=20% alt="Train">

[![status-badge](https://ci.codeberg.org/api/badges/rluetzner/train-bridge/status.svg)](https://ci.codeberg.org/rluetzner/train-bridge)

This is a simple web server that shows the next trains arriving or departing from a certain train station.

This relies on the DB (Deutsche Bahn) Timetables API and is therefore only applicable to train stations in Germany.

You'll need your own API key. Sign up [here](https://developers.deutschebahn.com).

## Motivation

My son likes to take a short walk and watch trains from a nearby bridge.
Until now I knew that there were four connections per hour, but I couldn't be bothered to remember when the trains would run.
With this web server I can see the next few trains at a quick glance, including the direction they are coming from.

## Usage

You can run the server with the following command.

```bash
TB_CLIENT_ID="your-client-id" TB_CLIENT_SECRET="your-client-secret" TB_EVA_NR="8000107" TB_NEXT_STATION="Freiburg-Wiehre" ./train-bridge
```

Or just pull the image from [Docker Hub](https://hub.docker.com/r/rluetzner/train-bridge).

```bash
docker run --detach \
    --env TB_CLIENT_ID="your-client-id" \
    --env TB_CLIENT_SECRET="your-client-secret" \
    --env TB_EVA_NR="8000107" \
    --env TB_NEXT_STATION="Freiburg-Wiehre" \
    --volume "/etc/timezone:/etc/timezone:ro" \
    --volume "/etc/localtime:/etc/localtime:ro" \
    --publish 8080:8080 \
    rluetzner/train-bridge
```

## Configuration

All configuration is done via environment variables.

| Env variable | Description | Example |
| ---------- | --------- | ----- |
| TB_CLIENT_ID | The Client ID of your application. You can create this in your DB developer space. | --- |
| TB_CLIENT_SECRET | The Client secret of your application. Created together with your Client ID. | --- |
| TB_EVA_NR | The EVA number of the train station. You can use the eva-tool in helper/eva-tool to find the number you need. | 8000107 |
| TB_NEXT_STATION | A string to look for. Results will be filtered to only include those connections with your "next station". | Freiburg-Wiehre |
| TB_LOG_LEVEL | A string to change the log level. Allowed values are: trace, debug, info, warn, error, fatal, panic. The default is info. | info |

## Attribution

| Source | Comment |
| ------ | ------- |
| [Train icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/train) | Used as favicon and as icon for this project. |
| [Tunnel icons created by Good Ware - Flaticon](https://www.flaticon.com/free-icons/tunnel) | Combined with the arrow for arrival and departure icons. |
|[Arrow icon created by iconsDB.com](https://www.iconsdb.com/black-icons/arrow-up-4-icon.html) | Combined with the tunnel for arrival and departure icons. |

package api

import (
	"errors"
	"math"
	"time"

	"codeberg.org/rluetzner/train-bridge/models"
	log "github.com/sirupsen/logrus"
)

type DbApiClientWithRetries struct {
	apiClient DbApiClient
	retries   int
}

func NewDbApiClientWithRetries(apiClient DbApiClient, retries int) *DbApiClientWithRetries {
	return &DbApiClientWithRetries{
		apiClient: apiClient,
		retries:   retries,
	}
}

func (c *DbApiClientWithRetries) GetTrains(date, hour, evaNr, nextStation string) ([]*models.Train, error) {
	collectedErrors := make([]error, 0)
	for i := range c.retries {
		trains, err := c.apiClient.GetTrains(date, hour, evaNr, nextStation)
		if err != nil {
			collectedErrors = append(collectedErrors, err)
			// Exponential backoff. Will give us:
			// 1, 2, 4, 8, 16, ... seconds.
			duration := time.Duration(math.Pow(2, float64(i)))
			log.Warnf("Encountered an error %s, will sleep for %d", err, duration)
			time.Sleep(duration * time.Second)
		} else {
			return trains, nil
		}
	}
	return nil, errors.Join(collectedErrors...)
}

func (c *DbApiClientWithRetries) GetStations(searchString string) (*models.Stations, error) {
	collectedErrors := make([]error, 0)
	for i := range c.retries {
		stations, err := c.apiClient.GetStations(searchString)
		if err != nil {
			collectedErrors = append(collectedErrors, err)
			// Exponential backoff. Will give us:
			// 1, 2, 4, 8, 16, ... seconds.
			duration := time.Duration(math.Pow(2, float64(i)))
			log.Warnf("Encountered an error %s, will sleep for %d", err, duration)
			time.Sleep(duration * time.Second)
		} else {
			return stations, nil
		}
	}
	return nil, errors.Join(collectedErrors...)
}

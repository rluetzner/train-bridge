package api

import (
	"fmt"
	"time"

	"codeberg.org/rluetzner/train-bridge/models"
	log "github.com/sirupsen/logrus"
)

type CachedDbApiClient struct {
	apiClient DbApiClient
	cache     map[string][]*models.Train
}

func NewCachedDbApiClient(apiClient DbApiClient) *CachedDbApiClient {
	return &CachedDbApiClient{
		apiClient: apiClient,
		cache:     make(map[string][]*models.Train),
	}
}

func (c *CachedDbApiClient) GetTrains(date, hour, evaNr, nextStation string) ([]*models.Train, error) {
	c.cleanCache()
	cacheKey := fmt.Sprintf("%s%s", date, hour)
	if c.cache[cacheKey] != nil {
		log.Infof("Serving %s from cache", cacheKey)
		return c.cache[cacheKey], nil
	}

	trains, err := c.apiClient.GetTrains(date, hour, evaNr, nextStation)
	if err != nil {
		return nil, err
	}
	c.cache[cacheKey] = trains
	return trains, nil
}

func (c *CachedDbApiClient) GetStations(searchString string) (*models.Stations, error) {
	return c.apiClient.GetStations(searchString)
}

func (c *CachedDbApiClient) cleanCache() {
	now := time.Now()
	date := now.Format(models.DateFormat)
	hour := fmt.Sprintf("%02d", now.Hour())
	currentCacheKey := fmt.Sprintf("%s%s", date, hour)

	var keysToRemove []string
	for k := range c.cache {
		if k < currentCacheKey {
			keysToRemove = append(keysToRemove, k)
		}
	}

	for _, k := range keysToRemove {
		delete(c.cache, k)
		log.Infof("Removed %s from cache", k)
	}
}

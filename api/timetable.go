package api

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"codeberg.org/rluetzner/train-bridge/errors"
	"codeberg.org/rluetzner/train-bridge/models"
	log "github.com/sirupsen/logrus"
)

type TimeTableClient struct {
	clientId     string
	clientSecret string
}

func NewTimeTable(c *models.ClientCredentials) *TimeTableClient {
	return &TimeTableClient{
		clientId:     c.ClientId,
		clientSecret: c.ClientSecret,
	}
}

func (c *TimeTableClient) GetTrains(date, hour, evaNr, nextStation string) ([]*models.Train, error) {
	const functionName = "GetTrains"
	url := fmt.Sprintf("%s/plan/%s/%s/%s", baseUrl, evaNr, date, hour)
	req, err := c.prepareRequest(url)
	if err != nil {
		return nil, errors.Wrap("failed to prepare request", functionName, err)
	}

	body, err := getResultBody(req)
	if err != nil {
		return nil, errors.Wrap("failed to get result body", functionName, err)
	}

	timetable, err := parseBody(body)
	if err != nil {
		return nil, errors.Wrap("failed to parse body", functionName, err)
	}

	trains := getMatchingTrainsFromTimetable(timetable, nextStation)
	return trains, nil
}

func (c *TimeTableClient) prepareRequest(url string) (*http.Request, error) {
	const functionName = "prepareRequest"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, errors.Wrap("unable to create new request", functionName, err)
	}

	req.Header.Add("DB-Client-Id", c.clientId)
	req.Header.Add("DB-Api-Key", c.clientSecret)
	req.Header.Add("accept", "application/xml")

	return req, nil
}

func getResultBody(req *http.Request) ([]byte, error) {
	const functionName = "getResultBody"
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.Wrap("failed to perform http request", functionName, err)
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)

	if err != nil {
		return nil, errors.Wrap("could not read response body", functionName, err)
	}
	return body, nil
}

func parseBody(body []byte) (*models.Timetable, error) {
	const functionName = "parseBody"
	var timetable models.Timetable
	err := xml.Unmarshal(body, &timetable)
	if err != nil {
		return nil, errors.Wrap("error trying to unmarshal XML", functionName, err)
	}
	return &timetable, nil
}

func getMatchingTrainsFromTimetable(timetable *models.Timetable, nextStation string) []*models.Train {
	trains := make([]*models.Train, 0)
	for _, stop := range timetable.Stops {
		if strings.Contains(stop.Arrival.PlannedPath, nextStation) {
			trains = append(trains, parseTrain(stop.Arrival.Connection, true))
		}
		if strings.Contains(stop.Departure.PlannedPath, nextStation) {
			trains = append(trains, parseTrain(stop.Departure.Connection, false))
		}
	}
	return trains
}

func parseTrain(connection models.Connection, isArrival bool) *models.Train {
	t, err := time.Parse(dateFormatWithTime, connection.PlannedTime)
	if err != nil {
		log.Warnf("Error parsing time '%s': %s", connection.PlannedTime, err)
	}
	return &models.Train{
		Time:      t.Format(SaneDateFormat),
		IsArrival: isArrival,
		Path:      strings.ReplaceAll(connection.PlannedPath, "|", ", "),
	}
}

func (c *TimeTableClient) GetStations(searchString string) (*models.Stations, error) {
	const functionName = "GetStations"
	url := fmt.Sprintf("%s/station/%s", baseUrl, searchString)
	req, err := c.prepareRequest(url)
	if err != nil {
		return nil, errors.Wrap("unable to prepare request", functionName, err)
	}
	res, err := getResultBody(req)
	if err != nil {
		return nil, errors.Wrap("failed to get result body", functionName, err)
	}
	return parseStations(res)
}

func parseStations(body []byte) (*models.Stations, error) {
	const functionName = "parseStations"
	var stations models.Stations
	err := xml.Unmarshal(body, &stations)
	if err != nil {
		log.Warnf("Unexpected XML: %s", string(body))
		return nil, errors.Wrap("unable to parse XML", functionName, err)
	}
	return &stations, nil
}

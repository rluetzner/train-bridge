package api

import "codeberg.org/rluetzner/train-bridge/models"

const (
	dateFormatWithTime = "0601021504"
	SaneDateFormat     = "2006-01-02 15:04"
	baseUrl            = "https://apis.deutschebahn.com/db-api-marketplace/apis/timetables/v1"
)

type DbApiClient interface {
	GetTrains(date, hour, evaNr, nextStation string) ([]*models.Train, error)
	GetStations(searchString string) (*models.Stations, error)
}

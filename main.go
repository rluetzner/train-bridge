package main

import (
	"fmt"
	"path/filepath"
	"strings"

	"codeberg.org/rluetzner/train-bridge/api"
	"codeberg.org/rluetzner/train-bridge/errors"
	"codeberg.org/rluetzner/train-bridge/logger"
	"codeberg.org/rluetzner/train-bridge/models"
	"codeberg.org/rluetzner/train-bridge/server"
)

func main() {
	logLevel, _ := models.GetLogLevel()
	logger.Init(logLevel)
	vars, err := models.GetEnvVars()
	if err != nil {
		logger.Fatal(err)
	}
	logger.LogStartupInfo(vars)
	initializeAndRunServer(vars)
}

func initializeAndRunServer(vars *models.EnvVars) {
	const functionName = "initializeAndRunServer"
	timeTableClient := api.NewTimeTable(vars.Credentials())
	retryClient := api.NewDbApiClientWithRetries(timeTableClient, 5)
	cachedClient := api.NewCachedDbApiClient(retryClient)

	stat, err := cachedClient.GetStations(vars.EvaNr)
	if err != nil {
		logger.Fatal(errors.Wrap("unable to get stations", functionName, err))
	}

	err = checkOnlyOneStationIsFound(stat)
	if err != nil {
		logger.Fatal(errors.Wrap("unable to check for only one station", functionName, err))
	}

	trainStation := stat.Station[0].Name
	logger.Info(fmt.Sprintf("Found train station: %s", trainStation))

	server := &server.Server{
		DbApiClient:    cachedClient,
		Settings:       models.NewSettings(vars.EvaNr, trainStation, vars.NextStation),
		TemplateFile:   filepath.Join("templates", "layout.html"),
		StaticFilesDir: "static",
	}

	server.Routes()
	server.Start()
}

func checkOnlyOneStationIsFound(stat *models.Stations) error {
	const functionName = "checkOnlyOneStationIsFound"
	if len(stat.Station) != 1 {
		stations := make([]string, 0)
		for _, s := range stat.Station {
			stations = append(stations, s.Name)
		}
		return errors.New(fmt.Sprintf("ambiguous station from EVA number: %s", strings.Join(stations, ", ")), functionName)
	}
	return nil
}

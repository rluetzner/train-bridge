module codeberg.org/rluetzner/train-bridge

go 1.23.0

toolchain go1.24.0

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/sirupsen/logrus v1.9.3
)

require golang.org/x/sys v0.31.0 // indirect

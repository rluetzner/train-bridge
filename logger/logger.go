package logger

import (
	"errors"

	tbErr "codeberg.org/rluetzner/train-bridge/errors"
	"codeberg.org/rluetzner/train-bridge/models"
	log "github.com/sirupsen/logrus"
)

type Log struct {
}

func Init(logLevel log.Level) {
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
	log.SetLevel(logLevel)
}

func LogStartupInfo(vars *models.EnvVars) {
	log.Infoln("Starting train-bridge server...")
	log.Infoln()
	log.Infof("TB_EVA_NR: %s", vars.EvaNr)
	log.Infof("TB_NEXT_STATION: %s", vars.NextStation)
	log.Infoln()
}

func Fatal(args ...any) {
	setCustomFieldsIfPresent(args)
	log.Fatal(args...)
}

func Error(args ...any) {
	setCustomFieldsIfPresent(args)
	log.Error(args...)
}

func Info(args ...any) {
	setCustomFieldsIfPresent(args)
	log.Info(args...)
}

func setCustomFieldsIfPresent(args []any) {
	var customError *tbErr.TbError
	for _, arg := range args {
		e, ok := arg.(error)
		if ok && errors.As(e, &customError) {
			log.WithFields(log.Fields{
				"StackTrace": customError.GetStackTrace(),
			})
			break
		}
	}
}

package main

import (
	"fmt"
	"os"
	"strings"

	"codeberg.org/rluetzner/train-bridge/api"
	"codeberg.org/rluetzner/train-bridge/models"
	"github.com/caarlos0/env"
	log "github.com/sirupsen/logrus"
)

func main() {
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
	logLevel, parsed := models.GetLogLevel()
	if parsed {
		log.SetLevel(logLevel)
	} else {
		// We don't need any info logs in this CLI tool.
		log.SetLevel(log.FatalLevel)
	}
	var credentials models.ClientCredentials
	_ = env.Parse(&credentials)
	checkArguments()

	searchStrings := getSearchStrings()

	timeTableClient := api.NewTimeTable(&credentials)

	stations, err := timeTableClient.GetStations("*")
	if err != nil {
		log.Fatalf("Unable to get stations: %s", err)
	}
	matchedStations := findMatches(stations, searchStrings)

	printMatches(matchedStations)
}

func checkArguments() {
	if len(os.Args) < 2 {
		fmt.Println("Outputting all stations.")
		fmt.Println("If you are looking for a specific station, please enter a search string as a parameter.")
	}
}

func getSearchStrings() []string {
	searchStrings := make([]string, 0)
	for _, arg := range os.Args[1:] {
		for _, s := range strings.Split(arg, " ") {
			searchStrings = append(searchStrings, strings.ToLower(s))
		}
	}
	log.Debugf("Search string: %v", searchStrings)
	return searchStrings
}

func findMatches(stations *models.Stations, searchStrings []string) []*models.Station {
	matchedStations := make([]*models.Station, 0)
	for _, station := range stations.Station {
		matches := true
		stationName := strings.ToLower(station.Name)
		for _, searchString := range searchStrings {
			if !strings.Contains(stationName, searchString) {
				matches = false
				break
			}
		}
		if matches {
			s := &models.Station{
				Name:  station.Name,
				EvaNr: station.Eva,
			}
			matchedStations = append(matchedStations, s)
		}
	}
	return matchedStations
}

func printMatches(matchedStations []*models.Station) {
	for _, match := range matchedStations {
		fmt.Printf("%s - %s", match.Name, match.EvaNr)
	}
}

# EVA-Tool

This tool helps you find out the EVA number you're looking for.

Although the DB API looks like it can be searched, at the time of writing it will only ever return a single result when a search string is entered.
The only way to make it return multiple results is to enter a wildcard '*'.

This tool will help you filter the returned results and quickly get the EVA number for a specific station.

## Usage

```bash
$ TB_CLIENT_ID="your-client-id" TB_CLIENT_SECRET="your-client-secret" ./eva-tool Freiburg breisgau
Search string: [freiburg breisgau]
Freiburg(Breisgau) Hbf - 8000107
Freiburg(Breisgau) Hbf/ZOB - 8089362
St. Georgen Kirche, Freiburg im Breisgau - 808262
St. Georgen Schiff, Freiburg im Breisgau - 808249
```

## Configuration

All configuration is done via environment variables.

| Env variable | Description | Example |
| ---------- | --------- | ----- |
| TB_CLIENT_ID | The Client ID of your application. You can create this in your DB developer space. | --- |
| TB_CLIENT_SECRET | The Client secret of your application. Created together with your Client ID. | --- |
| TB_LOG_LEVEL | A string to change the log level. Allowed values are: trace, debug, info, warn, error, fatal, panic. The default is info. | info |

## Install

Run the following command to install the tool:

```bash
go install codeberg.org/rluetzner/train-bridge/helper/eva-tool@latest
```
